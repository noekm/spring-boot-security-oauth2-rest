**REST, Spring Boot, Security, JPA, MySQL, OAuth2, JWT**

RESTful server which supports authentication with two levels of permission. 
All authenticated users are able to retrieve a greeting message.
Only admin user is able to retrieve a list of all usernames in the system.

**- Steps to Setup**

Prerequisites:
Java: 1.8.x 
Maven: 3.x.x 
MySQL: 5.x.x

1. Clone repo

2. Config database.

Change the spring.datasource.username and spring.datasource.password as per your MySQL installation in src/main/resources/application.properties.

3. Build and run the application

The app will start running at http://localhost:8080 

**- Steps to Run**

Explore the API endpoints

***Authentication***

``
curl -X POST \
  http://localhost:8080/oauth/token \
  -H 'authorization: Basic c2lnc2Vuc2UtY2xpZW50OnNpZ3NlbnNlLXNlY3JldA==' \
  -F grant_type=password \
  -F username='Savy Admin' \
  -F password='S3cuR3 P4ssW0rD' \
  -F client_id=sigsense-client
  -F client_secret=sigsense-secret
``

***Accessing secured endpoints***

``
  curl -X GET \
  http://localhost:8080/ \
  -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjI3Njc3OTUsInVzZXJfbmFtZSI6IlNhdnkgQWRtaW4iLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sImp0aSI6IjA0N2YyMTg2LTEyNzgtNDY2YS1iNmE2LWM5ZjIwZTY3NjFjOSIsImNsaWVudF9pZCI6InNpZ3NlbnNlLWNsaWVudCIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsInRydXN0Il19.YrlckxSphLAbkASQzm3yn9vQfs0fEf9Lg9BtN9Mg81c'
``

``
  curl -X GET \
  http://localhost:8080/users/ \
  -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjI3Njc3OTUsInVzZXJfbmFtZSI6IlNhdnkgQWRtaW4iLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sImp0aSI6IjA0N2YyMTg2LTEyNzgtNDY2YS1iNmE2LWM5ZjIwZTY3NjFjOSIsImNsaWVudF9pZCI6InNpZ3NlbnNlLWNsaWVudCIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsInRydXN0Il19.YrlckxSphLAbkASQzm3yn9vQfs0fEf9Lg9BtN9Mg81c'
``

*Using Google Postman:*

Go to Authorization Tab and select Type as Basic Auth
Fill the fields as follows:
username=sigsense-client 
password=sigsense-secret

POST Request Admin

``
http://localhost:8080/oauth/token?grant_type=password&username=Savy Admin&password=S3cuR3 P4ssW0rD
``

Copy the access_token returned in the response

GET Resquest to index (paste access_token into the param)

``
http://localhost:8080?access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjI3NjcyNTUsInVzZXJfbmFtZSI6IlNhdnkgQWRtaW4iLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sImp0aSI6ImMzOGVhZDA1LTViOWQtNDRlMC1hNTA2LWM3YzkxYTY5ZTcxYiIsImNsaWVudF9pZCI6InNpZ3NlbnNlLWNsaWVudCIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsInRydXN0Il19.GKik8Tne_S2M1jiJWVJ8UTTIFZLJydCLb_OJuTRV8-w
``

You will get a greeting: "Hello from Sigsense"

GET Resquest for users list

``
http://localhost:8080/users?access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjI3NjcyNTUsInVzZXJfbmFtZSI6IlNhdnkgQWRtaW4iLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sImp0aSI6ImMzOGVhZDA1LTViOWQtNDRlMC1hNTA2LWM3YzkxYTY5ZTcxYiIsImNsaWVudF9pZCI6InNpZ3NlbnNlLWNsaWVudCIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsInRydXN0Il19.GKik8Tne_S2M1jiJWVJ8UTTIFZLJydCLb_OJuTRV8-w
``

You get the list of all usernames in the system.
[
    "Best Candidate",
    "Savy Admin"
]

POST Request simple User

``
http://localhost:8080/oauth/token?grant_type=password&username=Best Candidate&password=Team Player
``

GET Resquest to index http://localhost:8080/

You will get the message: "Access is denied"



