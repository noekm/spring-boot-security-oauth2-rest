package com.mazzola.repository;

import static org.junit.Assert.assertNotNull;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.mazzola.Application;
import com.mazzola.model.User;
import com.mazzola.repository.UserRepository;

/**
 * @author Noelia Mazzola
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(locations = { "classpath:application-test.properties" })
@DataJpaTest
public class UserRepositoryTest {

	@Autowired
    private UserRepository userRepo;

    @Test
    public void whenFindByExistingUsername_thenSuccess() {
     
        // when
        User foundUser = userRepo.findByUsername("Best Candidate");
        // then
        assertNotNull(foundUser);
    }
    
    @Test
    public void whenFindUnknownByUsername_thenNotSuccess() {

        // when
        User foundUser = userRepo.findByUsername("Candidate");
        // then
        assertNull(foundUser);
    }
    
    @Test
    public void whenFindAllUsers_thenSuccess() {

        // when
        List<User> foundUsers = (List<User>) userRepo.findAll();
        // then
        assertTrue(foundUsers.size() != 0);
    }
}
