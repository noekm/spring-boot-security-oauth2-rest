package com.mazzola.service;

import static org.junit.Assert.assertTrue;

import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.mazzola.Application;
import com.mazzola.repository.UserRepository;
import com.mazzola.service.impl.UserServiceImpl;

/**
 * @author Noelia Mazzola
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(locations = { "classpath:application-test.properties" })
@DataJpaTest
public class UserServiceTest {

	@MockBean
	private UserRepository userRepository;

	@Autowired
	private UserService userService;
    
    @TestConfiguration
    static class ReservationServiceImplTestContextConfiguration {
    	@Bean
    	public UserService reservationService() {
    		return new UserServiceImpl();
    	}
    }
    
	@Test
	public void whenGetAllUsernames_thenReturnList() {
		
		String user = "User Test";
		String admin = "Admin Test";
		
		given(userRepository.findAllUsernames())
			.willReturn(Arrays.asList(user, admin));

		List<String> foundUsers = userService.getAllUsernames();
		
        assertTrue(foundUsers.size() != 0);
	}
}   
