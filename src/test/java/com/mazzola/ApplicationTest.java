package com.mazzola;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;

import java.util.Base64;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

/**
 * @author Noelia Mazzola
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations = { "classpath:application-test.properties" })
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = Application.class)
public class ApplicationTest {
	
	static final int ACCESS_TOKEN_VALIDITY_SECONDS = 30*60;

	@LocalServerPort
	private int port;

    private String clientBasicAuthCredentials;

    @Before
    public void setUp() {
        RestAssured.port = this.port;
        
        this.clientBasicAuthCredentials =
                Base64.getEncoder().encodeToString("sigsense-client:sigsense-secret".getBytes());
    }

    @Test
    public void foobarRequiresAuthorization() {
        when().
            get("/").
        then().
            statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    public void givenNoToken_whenGetRequest_thenUnauthorized() {
        when().
            get("/oauth/token").
        then().
            statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    public void givenNoOAuthParameters_whenGetRequest_thenNotAllowed() {
        given().
            header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
        when().
            get("/oauth/token").
        then().
            statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());
    }
    
    @Test
    public void givenTokenAndOAuthParameters_whenUnknownUserPostsRequest_thenBadRequest() {
        Response response =
            given().
                header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
                queryParam("username", "Fake User").
                queryParam("password", "Tpassword").
                queryParam("client_id", "sigsense-client").
                queryParam("grant_type", "password").
                queryParam("scope", "read").
            when().
                post("/oauth/token").
            then().
                statusCode(HttpStatus.BAD_REQUEST.value()).
                extract().response();

        Assert.assertEquals("Bad credentials", response.getBody().jsonPath().getString("error_description"));
    }

    @Test
    public void givenTokenAndOAuthParameters_whenUserPostsRequest_thenOKGrantAccess() {
        Response response =
            given().
                header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
                queryParam("username", "Best Candidate").
                queryParam("password", "Team Player").
                queryParam("client_id", "sigsense-client").
                queryParam("grant_type", "password").
                queryParam("scope", "read").
            when().
                post("/oauth/token").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        Assert.assertEquals("bearer", response.getBody().jsonPath().getString("token_type"));
        Assert.assertEquals("read", response.getBody().jsonPath().getString("scope"));
        Assert.assertEquals("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
                response.getBody().jsonPath().getString("access_token").split("[.]")[0]);
        
        
    }

    @Test
    public void givenUserWithGrantedAccess_whenGetIndex_thenGreetingFromSigsense() {
        Response tokenResponse =
            given().
                header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
                queryParam("username", "Best Candidate").
                queryParam("password", "Team Player").
                queryParam("client_id", "sigsense-client").
                queryParam("grant_type", "password").
                queryParam("scope", "read").
            when().
                post("/oauth/token").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        String token = tokenResponse.getBody().jsonPath().getString("access_token");

        Response requestResponse =
            given().
                header(new Header("Authorization", "Bearer " + token)).
            when().
                get("/").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        Assert.assertEquals("Hello from Sigsense", requestResponse.getBody().print());
    }
    
    @Test
    public void givenUserWithGrantedAccess_whenGetRequestUsersList_thenAccessDenied() {
        Response tokenResponse =
            given().
                header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
                queryParam("username", "Best Candidate").
                queryParam("password", "Team Player").
                queryParam("client_id", "sigsense-client").
                queryParam("grant_type", "password").
                queryParam("scope", "read").
            when().
                post("/oauth/token").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        String token = tokenResponse.getBody().jsonPath().getString("access_token");

        Response requestResponse =
            given().
                header(new Header("Authorization", "Bearer " + token)).
            when().
                get("/users").
            then().
                statusCode(HttpStatus.FORBIDDEN.value()).
                extract().response();

        Assert.assertEquals("Access is denied", requestResponse.getBody().jsonPath().getString("error_description"));
    }
    
    @Test
    public void givenAdminWithGrantedAccess_whenGetIndex_thenGreetingFromSigsense() {
        Response tokenResponse =
            given().
                header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
                queryParam("username", "Savy Admin").
                queryParam("password", "S3cuR3 P4ssW0rD").
                queryParam("client_id", "sigsense-client").
                queryParam("grant_type", "password").
                queryParam("scope", "read").
            when().
                post("/oauth/token").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        String token = tokenResponse.getBody().jsonPath().getString("access_token");

        Response requestResponse =
            given().
                header(new Header("Authorization", "Bearer " + token)).
            when().
                get("/").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        Assert.assertEquals("Hello from Sigsense", requestResponse.getBody().print());
    }
    
    @Test
    public void givenUserWithGrantedAccess_whenGetRequestUsersList_thenOkReturnUserList() {
        Response tokenResponse =
            given().
                header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
                queryParam("username", "Savy Admin").
                queryParam("password", "S3cuR3 P4ssW0rD").
                queryParam("client_id", "sigsense-client").
                queryParam("grant_type", "password").
                queryParam("scope", "read").
            when().
                post("/oauth/token").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        String token = tokenResponse.getBody().jsonPath().getString("access_token");

        Response requestResponse =
            given().
                header(new Header("Authorization", "Bearer " + token)).
            when().
                get("/users").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        Assert.assertNotNull(requestResponse.getBody().print().contains("Savy Admin"));
        Assert.assertNotNull(requestResponse.getBody().print().contains("Best Candidate"));
        
    }

    @Test
    public void givenUserWithTokenAndOAuthParam_whenGetsAccess_thenTokenExpiresIn30minutes() throws InterruptedException {
        Response tokenResponse =
            given().
                header(new Header("Authorization", "Basic " + this.clientBasicAuthCredentials)).
                queryParam("username", "Best Candidate").
                queryParam("password", "Team Player").
                queryParam("client_id", "sigsense-client").
                queryParam("grant_type", "password").
                queryParam("scope", "read").
            when().
                post("/oauth/token").
            then().
                statusCode(HttpStatus.OK.value()).
                extract().response();

        String expiresIn = tokenResponse.getBody().jsonPath().getString("expires_in");
        Assert.assertEquals(expiresIn, String.valueOf(ACCESS_TOKEN_VALIDITY_SECONDS - 1));

    }
}