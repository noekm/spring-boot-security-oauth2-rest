INSERT INTO Role (id, name) VALUES (1, 'ADMIN');
INSERT INTO Role (id, name) VALUES (2, 'USER');

INSERT INTO User (id, username, password) VALUES (1, 'Best Candidate', '$2a$10$wJFATA3Q5flj1cAlMUQVLe9jib/JDq3hqF/zij94Jub3BEozgKRM2');
INSERT INTO User (id, username, password) VALUES (2, 'Savy Admin', '$2a$10$R5P47LVT.pHeQONbjUSdNOCYLGnYD7KDcCoshp2vPkl8OloMxJKJG');

INSERT INTO user_role(USER_ID, role_ID) VALUES (2, 1);
INSERT INTO user_role(USER_ID, role_ID) VALUES (1, 2);