package com.mazzola.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mazzola.service.UserService;

/**
 * @author Noelia Mazzola
 */

@RestController
public class UserController {

    @Autowired
    private UserService userService;
    
    @GetMapping(value = "/")
    public String index(){
        return "Hello from Sigsense";
    }

    @RequestMapping(value="/users", method = RequestMethod.GET)
    public List<String> listUser(){	
    	return userService.getAllUsernames();
    }
}
