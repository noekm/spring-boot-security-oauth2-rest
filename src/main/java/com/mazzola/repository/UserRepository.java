package com.mazzola.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mazzola.model.User;

/**
 * @author Noelia Mazzola
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long>{
	
    User findByUsername(String username);
    
    @Query("SELECT u.username FROM User u ")
	List<String> findAllUsernames();

}
