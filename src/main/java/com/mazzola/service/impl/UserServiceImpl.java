package com.mazzola.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mazzola.model.User;
import com.mazzola.repository.UserRepository;
import com.mazzola.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Noelia Mazzola
 */

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
	private UserRepository userRepo;

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		User user = userRepo.findByUsername(userId);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.
				User(user.getUsername(), 
						user.getPassword(), 
						getAuthorities(user));
	}

	private List<GrantedAuthority> getAuthorities(User user) {

		return AuthorityUtils.createAuthorityList(
				user.getRoles()
				.stream()
				.map(r -> "ROLE_" + r.getName().toUpperCase())
				.collect(Collectors.toList())
				.toArray(new String[]{}));
	}

	public List<String> getAllUsernames() {

		return userRepo.findAllUsernames();
	}
}

