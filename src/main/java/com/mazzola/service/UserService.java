package com.mazzola.service;

import java.util.List;

/**
 * @author Noelia Mazzola
 */

public interface UserService {

    List<String> getAllUsernames();
}
